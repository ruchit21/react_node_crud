const UserAuth = require("./middleware/auth");
const PlanService = require("../services/plan-services");

module.exports = (app) => {
    const service = new PlanService();

    app.post("/create", async (req, res, next) => {
        try {
            const { plan_type, title, description, price, plan_duration, duration_type, type, status } = req.body;
            const data = await service.CreatePlan({ plan_type, title, description, price, plan_duration, duration_type, type, status });

            return res.json(data);
        } catch (error) {
            next(error.message)
        }
    });

    app.get("/plans", async (req, res, next) => {
        try {
            const data = await service.GetPlans();

            return res.json(data);
        } catch (error) {
            next(error.message);
        }
    })

    app.get("/plan/:id", async (req, res, next) => {
        try {
            const plan_id = req.params.id;
            const data = await service.GetSelectedPlan(plan_id);

            return res.json(data);
        } catch (error) {
            next(error.message);
        }
    });
};