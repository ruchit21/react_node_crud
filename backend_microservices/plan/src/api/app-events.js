// const UserController = require("../controller/user");
const PlanServices = require("../services/plan-services");

module.exports = (app) => {
  const service = new PlanServices();

  app.use("/app-events", async (req, res, next) => {
    const { payload } = req.body;

    service.SubscribeEvents(payload);

    return res.status(200).json(payload);
  });
};
