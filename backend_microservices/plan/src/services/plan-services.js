const { PlanRepository } = require("../database");
const {
  FormateData,
  GeneratePassword,
  GenerateSalt,
  GenerateSignature,
  ValidatePassword,
} = require("../utils");
const { APIError, BadRequestError } = require("../utils/app-errors");

// All Business logic will be here
class PlanService {
  constructor() {
    this.repository = new PlanRepository();
  }

  async CreatePlan(UserInputs) {
    try {
      const { plan_type, title, description, price, plan_duration, duration_type, type, status } = UserInputs;
      const plan = await this.repository.Create({ plan_type, title, description, price, plan_duration, duration_type, type, status: status.toString() });

      if (plan.status) {
        return { 'status': plan.status, 'message': plan.message, 'data': plan.data };
      } else {
        return { 'status': plan.status, 'message': plan.message, }
      }
    } catch (error) {
      throw new APIError(error.message)
    }
  }

  async GetPlans() {
    try {
      const plans = await this.repository.Plans();

      if (plans.status) {
        return { 'status': plans.status, 'message': plans.message, 'data': plans.data };
      } else {
        return { 'status': plans.status, 'message': plans.message, }
      }
    } catch (error) {
      throw new APIError(error.message);
    }
  }

  async GetSelectedPlan(id) {
    try {
      const plan = await this.repository.PlanFindById(id);

      if (plan.status) {
        return { 'status': plan.status, 'message': plan.message, 'data': plan.data };
      } else {
        return { 'status': plan.status, 'message': plan.message, }
      }
    } catch (error) {
      throw new APIError(error.message);
    }
  }
}

module.exports = PlanService;