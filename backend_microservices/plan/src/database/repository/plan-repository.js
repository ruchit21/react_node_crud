const { APIError, BadRequestError, STATUS_CODES } = require("../../utils/app-errors");
const db = require("../models");
const PlanModel = db.plan_model;

class PlanRepository {

    async Create({ plan_type, title, description, price, plan_duration, duration_type, type, status }) {
        try {
            const data = { plan_type, title, description, price, plan_duration, duration_type, type, status };

            const plan = await PlanModel.create(data);

            if (plan) {
                return { 'status': true, 'message': "The plan is created successfully.", 'data': plan };
            } else {
                return { 'status': false, 'message': "When making a plan, something is not right." }
            }
        } catch (error) {
            throw new APIError(error.message);
        }
    }

    async Plans() {
        try {
            const plans = await PlanModel.findAll();

            if (plans.length > 0) {
                return { 'status': true, 'message': "Plans were discovered successfully", 'data': plans }
            } else {
                return { 'status': false, 'message': "Plans could not be found" };
            }
        } catch (error) {
            throw new APIError(error.message);
        }
    }

    async PlanFindById(id) {
        try {
            const plan = await PlanModel.findOne({ where: { id: id } });

            if (plan) {
                return { 'status': true, 'message': "Plan were discovered successfully", 'data': plan }
            } else {
                return { 'status': false, 'message': "Plan could not be found" };
            }
        } catch (error) {
            throw new APIError(error.message)
        }
    }
}

module.exports = PlanRepository;