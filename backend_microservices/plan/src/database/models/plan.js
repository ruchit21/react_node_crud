
module.exports = (sequelize, Sequelize) => {
    const Plans = sequelize.define("plans", {
        id: {
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER,
        },
        plan_type: {
            type: Sequelize.ENUM,
            values: ["one time", "recurreing"],
            defaultValue: "one time"
        },
        title: {
            allowNull: false,
            type: Sequelize.STRING,
        },
        description: {
            allowNull: false,
            type: Sequelize.TEXT,
        },
        price: {
            allowNull: false,
            type: Sequelize.DECIMAL(10,2),
        },
        plan_duration: {
            allowNull: false,
            type: Sequelize.INTEGER
        },
        duration_type: {
            type: Sequelize.ENUM,
            values: ["min", "hour", "day", "month", "year"],
            defaultValue: "day"
        },
        product_id_android: {
            allowNull: true,
            type: Sequelize.STRING
        },
        product_id_ios: {
            allowNull: true,
            type: Sequelize.STRING
        },
        stripe_token: {
            allowNull: true,
            type: Sequelize.STRING
        },
        type: {
            type: Sequelize.ENUM,
            values: ["promotional", "subscription"],
            defaultValue: "promotional"
        },
        status: {
            type: Sequelize.ENUM,
            values: ["0", "1"],
            defaultValue: "0"
        }
    }, {
        timestamps: true,
        freezeTableName: true // Model tableName will be the same as the model name
    });
    return Plans;
};