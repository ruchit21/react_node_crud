const { UserRepository } = require("../database");
const {
  FormateData,
  GeneratePassword,
  GenerateSalt,
  GenerateSignature,
  ValidatePassword,
} = require("../utils");
const { APIError, BadRequestError } = require("../utils/app-errors");

// All Business logic will be here
class UserService {
  constructor() {
    this.repository = new UserRepository();
  }

  async SignUp(UserInputs) {
    try {
      const { first_name, last_name, contact_no, email, password, gender, country_id, zipcode, role } = UserInputs;
      let salt = await GenerateSalt();

      let UserPassword = await GeneratePassword(password, salt);

      const user = await this.repository.SignUp({
        first_name, last_name, contact_no, email, password: UserPassword, salt, gender, country_id, zipcode, role
      })

      if (user.status) {
        const token = await GenerateSignature({
          id: user.data.id,
          email: email,
          role: user.role
        });

        return FormateData({ 'status': user.status, 'message': user.message, 'data': { id: user.data.id, token } });
      }

      return FormateData(user);
    } catch (error) {
      throw new APIError(error.message);
    }
  }

  async LogIn(UserInputs) {
    try {
      const { email, password } = UserInputs;
      const user = await this.repository.FindUserByEmail(email);

      if (user.status) {
        const ValidPassword = await ValidatePassword(
          password,
          user.data.password,
          user.data.salt
        );

        if (ValidPassword) {
          const token = await GenerateSignature({
            id: user.data.id,
            email: user.data.email,
            role: user.data.role
          });

          return FormateData({ 'status': user.status, 'message': user.message, 'data': { id: user.data.id, token } });
        } else {
          return {'status': false, 'message': "You typed in the incorrect password."};
        }
      } else {
        return {'status': false, 'mesage': "This email address has not been used to register a user."}
      }
    } catch (error) {
      throw new APIError(error.message);
    }
  }

  async UserProfile(id) {
    try {
      const UserProfile = await this.repository.FindUserById(id);

      if (UserProfile.status) {
          return { 'status': UserProfile.status, 'message': UserProfile.message, 'data': UserProfile.data }
      } else {
        return { 'status': UserProfile.status, 'message': UserProfile.message }
      }
    } catch (error) {
      throw new APIError(error.message);
    }
  }

  async GetUserPayload(id, { userId }, event) {
    try {
      const user = await this.repository.FindUserById(id);

      if (user) {
        const payload = {
          event: event,
          user_data: { userId, user}
        }
        return { 'status': user.status, 'message': user.message, 'data': payload }
      } else {
        return { 'status': user.status, 'message': user.message }
      }
    } catch (error) {
      throw new APIError(error.message);
    }
  }
}

module.exports = UserService;