const { APIError, BadRequestError, STATUS_CODES } = require("../../utils/app-errors");
const db = require("../models");
const UserModel = db.user_model;

class UserRepository {
    async SignUp({ first_name, last_name, contact_no, email, password, salt, gender, country_id, zipcode, role }){
        try {
            const data = {
                first_name, last_name, contact_no, email, password, salt, gender, country_id, zipcode, role
            }

            const ExistingUser = await this.FindUserByEmail(email);

            if (ExistingUser.status) {
                return { 'status': false, 'message': "This email address has already been used by someone else. Please register using a different email." }
            } else {
                const user = await UserModel.create(data);
    
                if (user) {
                    return { 'status': true, 'message': "The user has successfully registered.", 'data': user };
                } else {
                    return { 'status': false, 'message': "The registration of the user was unsuccessful." }
                }
            }
        } catch (error) {
            throw new APIError(error.message)
        }
    }

    async FindUserByEmail(email) {
        try {
            const user = await UserModel.findOne({ where: { email: email } });

            if (user) {
                return { 'status': true, 'message': "The user was successfully located.", 'data': user }
            } else {
                return { 'status': false, 'message': "Unable to locate the user." }
            }
        } catch (error) {
            throw new APIError(error.message);
        }
    }

    async FindUserById(id) {
        try {
            const user = await UserModel.findOne({ where: { id: id } });

            if (user) {
                return { 'status': true, 'message': "The user was successfully located.", 'data': user };
            } else {
                return { 'status': false, 'message': "Unable to locate the user" };
            }
        } catch (error) {
            throw new APIError(error.message)
        }
    }
}

module.exports = UserRepository;