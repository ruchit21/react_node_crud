const UserAuth = require("./middleware/auth");
const UserService = require("../services/user-services");
const { PublishAddressEvent } = require("../utils");
const { UserSignUp, UserValidation, UserLogin } = require("./middleware/validation");

module.exports = (app) => {
    const service = new UserService();

    app.post("/signup", UserSignUp, UserValidation, async (req, res, next) => {
        try {
            const { first_name, last_name, contact_no, email, password, gender, country_id, zipcode, role } = req.body;
            const data = await service.SignUp({ first_name, last_name, contact_no, email, password, gender, country_id, zipcode, role });

            return res.json(data);
        } catch (error) {
            next(error.message)
        }
    });

    app.post("/login", UserLogin, UserValidation, async (req, res, next) => {
        try {
            const { email, password } = req.body;
            const data = await service.LogIn({ email, password });

            return res.json(data);
        } catch (error) {
            next(error.message)
        }
    });

    app.get("/profile", UserAuth,  async (req, res, next) => {
        try {
            const { id } = req.user;
            const data = await service.GetUserPayload(
                id,
                { userId: id },
                "profile"
            )
                
            const userData = await PublishAddressEvent(data);
            return res.json(userData);
        } catch (error) {
            next(error.message)
        }
    });
};