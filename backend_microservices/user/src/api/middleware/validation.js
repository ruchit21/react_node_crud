const { check, validationResult } = require("express-validator");

exports.UserSignUp = [
    check("first_name").trim().not().isEmpty().withMessage("First name is required"),
    check("last_name").trim().not().isEmpty().withMessage("Last name is required"),
    check("contact_no").trim().not().isEmpty().withMessage("Contact no is required").isNumeric().withMessage("Contact no must be integer").isLength({min: 10}).withMessage("Contact no must be atleast 10 digits"),
    check("email").normalizeEmail().isEmail().withMessage("Please enter valid email address"),
    check("password").trim().not().isEmpty().withMessage("password is required").isLength({min: 5}).withMessage("Password must be atleast 5 characters"),   
]

exports.UserLogin = [
    check("email").trim().not().isEmpty().withMessage("Email is required").isEmail().withMessage("Please enter valid email address"),
    check("password").trim().not().isEmpty().withMessage("Password is required").isLength({min:5}).withMessage("Password must be atleast 5 characters")
]

exports.UserValidation = (req, res, next) => {
    const result = validationResult(req).array();
    if (!result.length) return next();

    const error = result[0].msg;
    res.json({ status: false, message: error })
}