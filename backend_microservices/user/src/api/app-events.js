// const UserController = require("../controller/user");
const UserServices = require("../services/user-services");

module.exports = (app) => {
  const service = new UserServices();

  app.use("/app-events", async (req, res, next) => {
    const { payload } = req.body;

    service.SubscribeEvents(payload);

    return res.status(200).json(payload);
  });
};
