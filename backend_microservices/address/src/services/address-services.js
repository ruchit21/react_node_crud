const { AddressRepository } = require("../database");
const {
  FormateData,
  GeneratePassword,
  GenerateSalt,
  GenerateSignature,
  ValidatePassword,
} = require("../utils");
const { APIError, BadRequestError, AppError } = require("../utils/app-errors");

// All Business logic will be here
class AddressService {
  constructor() {
    this.repository = new AddressRepository();
  }

  async CreateAddress(UserInputs) {
    try {
      const { id, address, country_id, city, state, postal_code, default_address } = UserInputs;
      const data = await this.repository.Create({ id, address, country_id, city, state, postal_code, default_address: default_address.toString() });

      if (data.status) {
        return { 'status': data.status, 'message': data.message, 'data': data.data }
      } else {
        return { 'status': data.status, 'message': data.message }
      }
    } catch (error) {
      throw new APIError(error.message);
    }
  }

  async AddressList(id) {
    try {
      const data = await this.repository.Addresses(id);

      if (data.status) {
        return { 'status': data.status, 'message': data.message, 'data': data.data }
      } else {
        return { 'status': data.status, 'message': data.message }
      }
    } catch (error) {
      throw new APIError(error.message);
    }
  }

  async DefaultAddress(id) {
    try {
      const data = await this.repository.DefaultAddress(id);

      if (data.status) {
        return { 'status': data.status, 'message': data.message, 'data': data.data }
      } else {
        return { 'status': data.status, 'message': data.message }
      }
    } catch (error) {
      throw new APIError(error.message);
    }
  }

  async UserProfile(UserData) {
    try {
      const ProfileResults = await this.repository.UserProfile(UserData);

      if (ProfileResults) {
        return { 'status': ProfileResults.status, 'message': ProfileResults.message, 'data': ProfileResults }
      } else {
        return { 'status': ProfileResults.status, 'message': ProfileResults.message }
      }
    } catch (error) {
      throw new APIError(error.message);
    }
  }

  async Events(payload){
    try {
      const { data } = payload;
      const { event, user_data } = data;

      switch (event) {
        case "profile":
          const data = await this.UserProfile(user_data);
          return data.data;
          break;
      
        default:
          break;
      }
    } catch (error) {
      throw new APIError(error.message);
    }
  }
}

module.exports = AddressService;