const { StateRepository } = require("../database");
const {
  FormateData,
  GeneratePassword,
  GenerateSalt,
  GenerateSignature,
  ValidatePassword,
} = require("../utils");
const { APIError, BadRequestError, AppError } = require("../utils/app-errors");

// All Business logic will be here
class StateService {
  constructor() {
    this.repository = new StateRepository();
  }

  async Create(UserInputs) {
    try {
      const { name, country_id, status } = UserInputs;
      const data = await this.repository.Create({ name, country_id, status: status.toString() });

      if (data) {
        return { 'status': data.status, 'message': data.message, 'data': data.data };
      } else {
        return { 'status': data.status, 'message': data.message };
      }
    } catch (error) {
      throw new APIError(error.message);
    }
  }

  async StateList() {
    try {
      const data = await this.repository.StateList();

      if (data) {
        return { 'status': data.status, 'message': data.message, 'data': data.data };
      } else {
        return { 'status': data.status, 'message': data.message };
      }
    } catch (error) {
      throw new APIError(error.message);
    }
  }

  async ActiveState() {
    try {
      const data = await this.repository.ActiveState();

      if (data) {
        return { 'status': data.status, 'message': data.message, 'data': data.data };
      } else {
        return { 'status': data.status, 'message': data.message };
      }
    } catch (error) {
      throw new APIError(error.message)
    }
  }

  async StateFind(id) {
    try {
      const data = await this.repository.SelectedState(id);

      if (data) {
        return { 'status': data.status, 'message': data.message, 'data': data.data };
      } else {
        return { 'status': data.status, 'message': data.message };
      }      
    } catch (error) {
      throw new APIError(error.message)
    }
  }

  async StateDelete(id) {
    try {
      const data = await this.repository.Delete(id);

      if (data) {
        return { 'status': data.status, 'message': data.message, 'data': data.data };
      } else {
        return { 'status': data.status, 'message': data.message };
      }
    } catch (error) {
      throw new APIError(error.message);
    }
  }
}

module.exports = StateService;