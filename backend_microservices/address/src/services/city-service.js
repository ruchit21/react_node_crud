const { CityRepository } = require("../database");
const {
  FormateData,
  GeneratePassword,
  GenerateSalt,
  GenerateSignature,
  ValidatePassword,
} = require("../utils");
const { APIError, BadRequestError, AppError } = require("../utils/app-errors");

// All Business logic will be here
class CityService {
  constructor() {
    this.repository = new CityRepository();
  }

  async CreateCity(UserInputs) {
      try {
        const { name, state_id, status } = UserInputs;
        const data = await this.repository.CreateCity({ name, state_id, status: status.toString() });

        if (data) {
            return { 'status': data.status, 'message': data.message, 'data': data.data };
        } else {
            return { 'status': data.status, 'message': data.message };
        }
      } catch (error) {
        throw new APIError(error.message);
      }
  }

  async GetAllCity() {
      try {
          const data = await this.repository.AllCityList();

        if (data) {
            return { 'status': data.status, 'message': data.message, 'data': data.data };
        } else {
            return { 'status': data.status, 'message': data.message };
        }
      } catch (error) {
          throw new APIError(error.message);
      }
  }

  async GetAllActiveCity() {
      try {
        const data = await this.repository.AllActiveCity();

        if (data) {
            return { 'status': data.status, 'message': data.message, 'data': data.data };
        } else {
            return { 'status': data.status, 'message': data.message };
        }
      } catch (error) {
          throw new APIError(error.message)
      }
  }

  async GetSelectedCity(id) {
      try {
        const data = await this.repository.GetCity(id);

        if (data) {
            return { 'status': data.status, 'message': data.message, 'data': data.data };
        } else {
            return { 'status': data.status, 'message': data.message };
        }
      } catch (error) {
          throw new APIError(error.message);
      }
  }

  async DeleteCity(id) {
      try {
          const data = await this.repository.Delete(id);

        if (data) {
            return { 'status': data.status, 'message': data.message, 'data': data.data };
        } else {
            return { 'status': data.status, 'message': data.message };
        }
      } catch (error) {
          throw new APIError(error.message);
      }
  }
}

module.exports = CityService;