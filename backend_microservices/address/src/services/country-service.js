const { CountryRepository } = require("../database");
const {
  FormateData,
  GeneratePassword,
  GenerateSalt,
  GenerateSignature,
  ValidatePassword,
} = require("../utils");
const { APIError, BadRequestError, AppError } = require("../utils/app-errors");

// All Business logic will be here
class CountryService {
  constructor() {
    this.repository = new CountryRepository();
  }

  async CreateCountry(UserInputs){
    try {
      const { country_code, name, status } = UserInputs;
      const data = await this.repository.CountryCreate({ country_code, name, status });

      if (data.status) {
        return { 'status': data.status, 'message': data.message, 'data': data.data }
      } else {
        return { 'status': data.status, 'message': data.message }
      }
    } catch (error) {
      throw new APIError(error.message);
    }
  }

  async GetCountryList() {
    try {
      const data = await this.repository.CountryList();

      if (data.status) {
        return { 'status': data.status, 'message': data.message, 'data': data.data }
      } else {
        return { 'status': data.status, 'message': data.message }
      }
    } catch (error) {
      throw new APIError(error.message);
    }
  }

  async CountryActive() {
      try {
          const data = await this.repository.ActiveCountryList();

          if (data.status) {
            return { 'status': data.status, 'message': data.message, 'data': data.data }
          } else {
            return { 'status': data.status, 'message': data.message }
          }
      } catch (error) {
          throw new APIError(error.message);
      }
  }

  async CountryFind(id) {
    try {
      const data = await this.repository.CountryFind(id);

      if (data.status) {
        return { 'status': data.status, 'message': data.message, 'data': data.data }
      } else {
        return { 'status': data.status, 'message': data.message }
      }
    } catch (error) {
      throw new APIError(error.message);
    }
  }

  async CountryDelete(id) {
    try {
      const data = await this.repository.CountryDelete(id);

      if (data.status) {
        return { 'status': data.status, 'message': data.message, 'data': data.data }
      } else {
        return { 'status': data.status, 'message': data.message }
      }
    } catch (error) {
      throw new APIError(error.message);
    }
  }
}

module.exports = CountryService;