
module.exports = (sequelize, Sequelize) => {
    const City = sequelize.define("city", {
        id: {
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER,
        },
        name: {
            allowNull: false,
            type: Sequelize.STRING
        },
        state_id: {
            allowNull: false,
            type: Sequelize.INTEGER
        },
        status: {
            allowNull: false,
            type: Sequelize.INTEGER
        }
    }, {
        timestamps: false,
        freezeTableName: true // Model tableName will be the same as the model name
    });
    return City;
};