
module.exports = (sequelize, Sequelize) => {
    const Address = sequelize.define("address", {
        id: {
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER,
        },
        user_id: {
            allowNull: false,
            type: Sequelize.INTEGER
        },
        address: {
            allowNull: false,
            type: Sequelize.STRING,
        },
        country_id: {
            allowNull: false,
            type: Sequelize.INTEGER
        },
        city: {
            allowNull: false,
            type: Sequelize.STRING
        },
        state: {
            allowNull: false,
            type: Sequelize.STRING
        },
        postal_code: {
            allowNull: false,
            type: Sequelize.INTEGER
        },
        default_address: {
            type: Sequelize.ENUM,
            values: ["0", "1"],
            defaultValues: "0"
        }
    }, {
        timestamps: true,
        freezeTableName: true // Model tableName will be the same as the model name
    });
    return Address;
};