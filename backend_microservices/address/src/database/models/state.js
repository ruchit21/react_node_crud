
module.exports = (sequelize, Sequelize) => {
    const State = sequelize.define("state", {
        id: {
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER,
        },
        name: {
            allowNull: false,
            type: Sequelize.STRING
        },
        country_id: {
            allowNull: false,
            type: Sequelize.INTEGER
        },
        status: {
            allowNull: false,
            type: Sequelize.STRING
        }
    }, {
        timestamps: false,
        freezeTableName: true // Model tableName will be the same as the model name
    });
    return State;
};