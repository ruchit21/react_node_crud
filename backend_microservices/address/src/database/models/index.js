const config = require("../../config");

const Sequelize = require("sequelize");
const sequelize = new Sequelize(
    config.db,
    config.user,
    config.password,
    {
        host: config.host,
        dialect: config.dialect,
        operatorsAliases: false,
    }
);

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.address_model           = require("./address")(sequelize, Sequelize);
db.country_model           = require("./country")(sequelize, Sequelize);
db.state_model             = require("./state")(sequelize, Sequelize);
db.city_model             = require("./city")(sequelize, Sequelize);

module.exports = db;