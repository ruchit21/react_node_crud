
module.exports = (sequelize, Sequelize) => {
    const Country = sequelize.define("country", {
        id: {
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER,
        },
        country_code: {
            allowNull: false,
            type: Sequelize.STRING
        },
        name: {
            allowNull: false,
            type: Sequelize.STRING
        },
        defaultLanguageId: {
            allowNull: true,
            type: Sequelize.STRING
        },
        status: {
            allowNull: false,
            type: Sequelize.INTEGER
        }
    }, {
        timestamps: false,
        freezeTableName: true // Model tableName will be the same as the model name
    });
    return Country;
};