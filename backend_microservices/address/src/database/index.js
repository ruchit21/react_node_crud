// database related modules
module.exports = {
    AddressRepository: require('./repository/address-repository'),
    CountryRepository: require('./repository/country-repository'),
    StateRepository: require('./repository/state-repository'),
    CityRepository: require('./repository/city-repository'),
}