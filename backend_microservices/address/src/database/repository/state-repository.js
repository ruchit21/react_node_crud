const { APIError, BadRequestError, STATUS_CODES } = require("../../utils/app-errors");
const db = require("../models");
const StateModel = db.state_model;

class StateRepository {
    async Create({ name, country_id, status }) {
        try {
            const data = { name, country_id, status };
            const state = await StateModel.create(data);

            if (state) {
                return { 'status': true, 'message': "The state has been successfully created.", 'data': state };
            } else {
                return { 'status': false, 'message': "The state was not successfully created." }
            }
        } catch (error) {
            throw new APIError(error.message);
        }
    }

    async StateList() {
        try {
            const state = await StateModel.findAll();

            if (state) {
                return { 'status': true, 'message': "The states was successfully found.", 'data': state }
            } else {
                return { 'status': false, 'message': "The states was not found." }
            }
        } catch (error) {
            throw new APIError(error.message);
        }
    }

    async ActiveState() {
        try {
            const state = await StateModel.findAll({ where: { status: '1' } });

            if (state) {
                return { 'status': true, 'message': "The active states has been descovered.", 'data': state }
            } else {
                return { 'status': false, 'message': "The active states has been not descovered." };
            }
        } catch (error) {
            throw new APIError(error.message);
        }
    }

    async SelectedState(id) {
        try {
            const state = await StateModel.findOne({ where: { id: id } });

            if (state) {
                return { 'status': true, 'message': "State data was discovered successfully.", 'data': state }
            } else {
                return { 'status': false, 'message': "State data could not be discovered." }
            }
        } catch (error) {
            throw new APIError(error.message);
        }
    }

    async Delete(id) {
        try {
            const state = await StateModel.findOne({ where: { id: id } });

            if (state) {
                await state.destroy()
                return { 'status': true, 'message': "State data was successfully removed.", 'data': state }
            } else {
                return { 'status': false, 'message': "State data could not be found." }
            }
        } catch (error) {
            throw new APIError(error.message);
        }
    }
}

module.exports = StateRepository;