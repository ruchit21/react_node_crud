const { APIError, BadRequestError, STATUS_CODES } = require("../../utils/app-errors");
const db = require("../models");
const AddressModel = db.address_model;
const CountryModel = db.country_model;

class AddressRepository {

    async Create({ id, address, country_id, city, state, postal_code, default_address }) {
        try {
            const country = await CountryModel.findOne({ where: { name: country_id } });
            if (!country) {
                return { 'status': false, 'message': "This nation is not currently accessible." }
            } 
            const data = { user_id: id, address, country_id: country.id, city, state, postal_code, default_address };
            let isExist = false;
            let address_data = [];

            const addresses = await AddressModel.findAll({ where: { user_id: id } });

            if (addresses.length > 0) {
                addresses.map(async (user_address) => {
                    if (user_address.default_address.toString() == "1" && default_address.toString() == "1") {
                        isExist = true;
                        const update_address = await AddressModel.update({ default_address: '0' }, {where: { id: user_address.id }});
                        address_data.push(update_address);
                    } else {
                        const new_add = new AddressModel({
                            user_id: id,
                            address,
                            country_id: country.id,
                            city,
                            state,
                            postal_code,
                            default_address
                        });
                        address_data.push(new_add);
                        await new_add.save();
                    }
                });
                return { 'status': true, 'message': "Address has been successfully created.", 'data': address_data };
            } else {
                const user_address = await AddressModel.create(data);
    
                if (user_address) {
                    return { 'status': true, 'message': "Address has been successfully created.", 'data': user_address };
                } else {
                    return { 'status': false, 'message': "No address has been created." }
                }
            }   
        } catch (error) {
            throw new APIError(error.message)
        }
    }

    async Addresses(id) {
        try {
            const address = await AddressModel.findAll({ where: { user_id: id } });

            if (address) {
                return { 'status': true, 'message': "The addresses are successfully found.", 'data': address };
            } else {
                return { 'status': false, 'message': "There are no addresses found." }
            }
        } catch (error) {
            throw new APIError(error.message);
        }
    }

    async DefaultAddress(id) {
        try {
            const default_address = await AddressModel.findOne({ where: { id: id, default_address: '1' } });

            if (default_address) {
                return { 'status': true, 'message': "The default address was discovered successfully.", 'data': default_address }
            } else {
                return { 'status': false, 'message': "The default address could not be found." }
            }
        } catch (error) {
            throw new APIError(error.message);
        }
    }

    async UserProfile(UserData) {
        try {
            const { userId, user } = UserData;
            const { data } = user;
            const Data = [];

            const address = await AddressModel.findAll({ where: { user_id: userId } });

            if (address.length > 0) {
                Data.push({ 'user_data': user.data, 'user_addresses': address });
                return { 'status': true, 'message': "User profile get successfully", 'data': Data }
            } else {
                data.push({ 'user_data': user.data });
                return { 'status': true, 'message': "User profile get successfully", 'data': Data }
            }
        } catch (error) {
            throw new APIError(error.message);
        }
    }

}

module.exports = AddressRepository;