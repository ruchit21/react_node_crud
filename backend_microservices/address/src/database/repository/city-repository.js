const { APIError, BadRequestError, STATUS_CODES } = require("../../utils/app-errors");
const db = require("../models");
const CityModel = db.city_model;

class CityRepository {

    async CreateCity({ name, state_id, status }) {
        try {
            const data = {
                name,
                state_id,
                status
            }

            const city = await CityModel.create(data);
            if (city) {
                return { 'status': true, 'message': "City data created successfully.", 'data': city }
            } else {
                return { 'status': false, 'message': "City data not created successfully." }
            }
        } catch (error) {
            throw new APIError(error.message)
        }
    }

    async AllCityList() {
        try {
            const cities = await CityModel.findAll();

            if (cities) {
                return { 'status': true, 'message': "Data of the city was discovered successfully.", 'data': cities }
            } else {
                return { 'status': false, 'message': "Data of the city was not found." }
            }
        } catch (error) {
            throw new APIError(error.message);
        }
    }

    async AllActiveCity() {
        try {
            const cities = await CityModel.findAll({ where: { status: "1" } });

            if (cities) {
                return { 'status': true, 'message': "The active cities has been descovered.", 'data': cities }
            } else {
                return { 'status': false, 'message': "The active cities has been not descovered." }
            }
        } catch (error) {
            throw new APIError(error.message);
        }
    }

    async GetCity(id) {
        try {
            const city = await CityModel.findOne({ where: { id: id } });

            if (city) {
                return { 'status': true, 'message': "City data was discovered successfully.", 'data': city }
            } else {
                return { 'status': false, 'message': "City data could not be discovered." }
            }
        } catch (error) {
            throw new APIError(error.message);
        }
    }

    async Delete(id) {
        try {
            const city = await CityModel.findOne({ where: { id: id } });

            if (city) {
                await city.destroy();
                return { 'status': true, 'message': "City data was successfully removed.", 'data': city }
            } else {
                return { 'status': false, 'message': "City data could not be found." }
            }
        } catch (error) {
            throw new APIError(error.message);
        }
    }

}

module.exports = CityRepository;