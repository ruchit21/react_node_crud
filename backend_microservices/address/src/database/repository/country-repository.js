const { APIError, BadRequestError, STATUS_CODES } = require("../../utils/app-errors");
const db = require("../models");
const CountryModel = db.country_model;

class CountryRepository {

    async CountryCreate({ country_code, name, status }){
        try {
            const data = { country_code, name, status };
            const country = await CountryModel.create(data);

            if (country) {
                return { 'status': true, 'message': "Country created successfully.", 'data': country }
            } else {
                return { 'status': false, 'message': "Something went wrong during the creation of the country." }
            }
        } catch (error) {
            throw new APIError(error.message)
        }
    }

    async CountryList() {
        try {
            const countries = await CountryModel.findAll();

            if (countries) {
                return { 'status': true, 'message': "Info on nations is successfully located.", 'data': countries }
            } else {
                return { 'status': false, 'message': "Data on nations could not be found." }
            }
        } catch (error) {
            throw new APIError(error.message);
        }
    }

    async ActiveCountryList() {
        try {
            const country = await CountryModel.findAll({ where: { status: "1" } });

            if (country) {
                return { 'status': true, 'message': "Successfully discovered active country.", 'data': country }
            } else {
                return { 'status': false, 'message': "There is no country that is active." }
            }
        } catch (error) {
            throw new APIError(error.message);
        }
    }

    async CountryFind(id) {
        try {
            const country = await CountryModel.findOne({ where: { id: id } });

            if (country) {
                return { 'status': true, 'message': "Info on nation is successfully located.", 'data': country }   
            } else {
                return { 'status': false, 'message': "Data on nation could not be found." }
            }
        } catch (error) {
            throw new APIError(error.message)
        }
    }

    async CountryDelete(id) {
        try {
            const country = await CountryModel.findOne({ where: { id: id } });
            
            if (country) {
                await country.destroy();
                return { 'status': true, 'message': "Data on nations was successfully erased.", 'data': country };
            } else {
                return { 'status': false, 'message': "Data on nation could not be found." }
            }
        } catch (error) {
            throw new APIError(error.message);
        }
    }

}

module.exports = CountryRepository;