const dotEnv = require("dotenv");

if (process.env.NODE_ENV !== "prod") {
  const configFile = `./.env.${process.env.NODE_ENV}`;
  dotEnv.config({ path: configFile });
} else {
  dotEnv.config();
}

module.exports = {
  host: "localhost",
  user: "root",
  password: "",
  db: "node_crud",
  dialect: "mysql",
  PORT: 5003,
  APP_SECRET: 'microservices_tutorial'
};
