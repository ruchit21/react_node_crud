const { check, validationResult } = require("express-validator");

exports.AddressCreate = [
    check("address").trim().not().isEmpty().withMessage("Address is required"),
    check("country_id").trim().not().isEmpty().withMessage("Country id is required"),
    check("city").trim().not().isEmpty().withMessage("City is required"),
    check("state").trim().not().isEmpty().withMessage("State is required"),
    check("postal_code").trim().not().isEmpty().withMessage("Postal code is required").isNumeric().withMessage("Postal code must be an integer"),
    check("default_address").trim().not().isEmpty().withMessage("Default address is required")
]

exports.CountryCreate = [
    check("country_code").trim().not().isEmpty().withMessage("Country code is required"),
    check("name").trim().not().isEmpty().withMessage("Name is required"),
    check("status").trim().not().isEmpty().withMessage("Status is required").isNumeric().withMessage("Status is must be an integer")
]

exports.StateCreate = [
    check("name").trim().not().isEmpty().withMessage("Name is required"),
    check("country_id").trim().not().isEmpty().withMessage("Country id is required"),
    check("status").trim().not().isEmpty().withMessage("Status is required").isNumeric().withMessage("Status must be an integer")
]

exports.CityCreate = [
    check("name").trim().not().isEmpty().withMessage("Name is required"),
    check("state_id").trim().not().isEmpty().withMessage("State id is required"),
    check("status").trim().not().isEmpty().withMessage("Status is required").isNumeric().withMessage("Status must be an integer")
]

exports.AddressValidation = (req, res, next) => {
    const result = validationResult(req).array();
    if (!result.length) return next();

    const error = result[0].msg;
    res.json({ status: false, message: error });
}