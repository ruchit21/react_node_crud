// const UserController = require("../controller/user");
const AddressServices = require("../services/address-services");

module.exports = (app) => {
  const service = new AddressServices();

  app.use("/app-events", async (req, res, next) => {
    const { payload } = req.body;
    const data = await service.Events(payload);

    return res.status(200).json(data);
  });
};
