const UserAuth = require("./middleware/auth");
const AddressService = require("../services/address-services");
const CountryService = require("../services/country-service");
const StateService = require("../services/state-service");
const CityService = require("../services/city-service");
const { AddressCreate, AddressValidation, CountryCreate, StateCreate, CityCreate } = require("./middleware/validation");

module.exports = (app) => {
    const Address_Service = new AddressService();
    const Country_Service = new CountryService();
    const State_Service = new StateService();
    const City_Service = new CityService();

    // Address API 

    app.post("/create", AddressCreate, AddressValidation, UserAuth, async (req, res, next) => {
        try {
            const { id } = req.user;
            const { address, country_id, city, state, postal_code, default_address } = req.body;

            const data = await Address_Service.CreateAddress({ id, address, country_id, city, state, postal_code, default_address });

            return res.json(data);
        } catch (error) {
            next(error.message)
        }
    });

    app.get("/address-list", UserAuth, async (req, res, next) => {
        try {
            const { id } = req.user;
            const data = await Address_Service.AddressList(id);

            return res.json(data);
        } catch (error) {
            next(error.message)
        }
    });

    app.get("/default-address", UserAuth, async (req, res, next) => {
        try {
            const { id } = req.user;
            const data = await Address_Service.DefaultAddress(id);

            return res.json(data);
        } catch (error) {
            next(error.message)
        }
    });

    // Country API 

    app.post("/country-create", CountryCreate, AddressValidation, async (req, res, next) => {
        try {
            const { country_code, name, status } = req.body;
            const data = await Country_Service.CreateCountry({ country_code, name, status: status.toString() });

            return res.json(data);
        } catch (error) {
            next(error.next);
        }
    });

    app.get("/country-list", async (req, res, next) => {
        try {
            const data = await Country_Service.GetCountryList();

            return res.json(data);
        } catch (error) {
            next(error.message)
        }
    });

    app.get("/country-active", async (req, res, next) => {
        try {
            const data = await Country_Service.CountryActive();

            return res.json(data);
        } catch (error) {
            next(error.message);
        }
    });

    app.get("/country/:id", async (req, res, next) => {
        try {
            const country_id = req.params.id;
            const data = await Country_Service.CountryFind(country_id);

            return res.json(data);
        } catch (error) {
            next(error.message);
        }
    });

    app.delete("/country-delete/:id", async (req, res, next) => {
        try {
            const country_id = req.params.id;
            const data = await Country_Service.CountryDelete(country_id);

            return res.json(data);
        } catch (error) {
            next(error.message);
        }
    });

    // State API 

    app.post("/state-create", StateCreate, AddressValidation, async (req, res, next) => {
        try {
            const { name, country_id, status } = req.body;
            const data = await State_Service.Create({ name, country_id, status });

            return res.json(data);
        } catch (error) {
            next(error.message);
        }
    });

    app.get("/state-list", async (req, res, next) => {
        try {
            const data = await State_Service.StateList();

            return res.json(data);
        } catch (error) {
            next(error.message);
        }
    })

    app.get("/state-active", async (req, res, next) => {
        try {
            const data = await State_Service.ActiveState();

            return res.json(data);
        } catch (error) {
            next(error.message);
        }
    })

    app.get("/state/:id", async (req, res, next) => {
        try {
            const state_id = req.params.id;
            const data = await State_Service.StateFind(state_id);

            return res.json(data);
        } catch (error) {
            next(error.message)
        }
    })

    app.delete("/state-delete/:id", async (req, res, next) => {
        try {
            const state_id = req.params.id;
            const data = await State_Service.StateDelete(state_id);

            return res.json(data);
        } catch (error) {
            next(error.message);
        }
    });

    // City API

    app.post("/city-create", CityCreate, AddressValidation, async (req, res, next) => {
        try {
            const { name, state_id, status } = req.body;
            const data = await City_Service.CreateCity({ name, state_id, status });

            return res.json(data);
        } catch (error) {
            next(error.message);
        }
    });

    app.get("/city-list", async (req, res, next) => {
        try {
            const data = await City_Service.GetAllCity();

            return res.json(data);
        } catch (error) {
            next(error.message);
        }
    });

    app.get("/city-active", async (req, res, next) => {
        try {
            const data = await City_Service.GetAllActiveCity();

            return res.json(data);
        } catch (error) {
            next(error.message)
        }
    });

    app.get("/city/:id", async (req, res, next) => {
        try {
            const city_id = req.params.id;
            const data = await City_Service.GetSelectedCity(city_id);

            return res.json(data);
        } catch (error) {
            next(error.message);
        }
    })

    app.delete("/city-delete/:id", async (req, res, next) => {
        try {
            const city_id = req.params.id;
            const data = await City_Service.DeleteCity(city_id);
            
            return res.json(data);
        } catch (error) {
            next(error.message)
        }
    })
};