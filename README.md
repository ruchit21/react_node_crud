# Node CURD

## Installation

1. Clone This Project using SSH or HTTPS Link
2. By default folder name will react_node_crud. (You can change it)

## RUN COMMAND

3. Setup node js

```bash
cd backend

npm i

cd ../frontend

npm i
```

## DATABASE

4. Set up database connection in backend/config/db.config.js
5. Import node_crud.sql file in your database.

## Setup Completed

## RUN COMMAND

6. To Start Program

```bash

npm start

```

7. Open Another Terminal

```bash

cd ../backend

nodemon

```
