module.exports = (sequelize, Sequelize) => {
  const Setting = sequelize.define(
    "setting",
    {
      id: {
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      name: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      slug: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      value: {
        allowNull: false,
        type: Sequelize.STRING,
      },
    },
    {
      timestamps: true,
      freezeTableName: true, // Model tableName will be the same as the model name
    }
  );
  return Setting;
};
