module.exports = (sequelize, Sequelize) => {
  const Pincode = sequelize.define(
    "pincode",
    {
      id: {
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      pincode: {
        allowNull: false,
        type: Sequelize.INTEGER,
      },
      city_id: {
        allowNull: false,
        type: Sequelize.INTEGER,
      },
    },
    {
      underscored: true,
      freezeTableName: true, // Model tableName will be the same as the model name
    }
  );
  return Pincode;
};
