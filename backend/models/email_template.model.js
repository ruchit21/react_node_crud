module.exports = (sequelize, Sequelize) => {
  const EmailTemplate = sequelize.define(
    "email_template",
    {
      id: {
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      email_template_name: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      email_status: {
        type: Sequelize.ENUM,
        values: ['active', 'inactive'],
        defaultValue: 'active'
      },
      email_slug: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      email_content: {
        allowNull: true,
        type: Sequelize.TEXT,
      },
      email_subject: {
        allowNull: true,
        type: Sequelize.STRING,
      },
      email_available_tags: {
        allowNull: true,
        type: Sequelize.TEXT,
      },
    },
    {
      timestamps: false,
      freezeTableName: true, // Model tableName will be the same as the model name
    }
  );
  return EmailTemplate;
};
