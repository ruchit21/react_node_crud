const db = require("../models");
const md5 = require("react-native-md5");
const await = require("await");
const path = require("path");

const User = db.user_model;

exports.createUser = async function (req, res) {
    let data = {
        first_name: req.body.first_name,
        last_name: req.body.last_name,
        contact_no: Number(req.body.contact_no),
        email: req.body.email,
        gender: req.body.gender,
        // image: req.file.filename,
        image: req.body.image,
        country_id:req.body.country_id,
        zipcode:req.body.zipcode,
        password: md5.hex_md5(req.body.password),
    }
    
    const user = await User.create(data);

    res.status(200).json({
        status : true,
        message: "User Created Successfully",
        data: user,
    });
};

exports.getUsers = async function (req, res) {
    const user = await User.findAll()
    
    if(!user) {
        res.status(200).json({
            status : false,
            message: "No Data Found.",
            data: user,
        });
    }

    res.status(200).json({
        status : true,
        message: "Data get Successfully",
        data: user,
    });
}

exports.getUser = async function (req, res) {
    const id = req.params.id;
    const user = await User.findOne({where: {id: id}})

    if(!user) {
        res.status(200).json({
            status : false,
            message: "No Data Found.",
            data: user,
        });
    }

    res.status(200).json({
        status : true,
        message: "Data get Successfully",
        data: user,
    });
}

//update the users at admin side
exports.updateUser = async function (req, res) {
    const id = req.params.id;

    const user = await User.update(req.body, {where: {id: id}});
    const getuser = await User.findOne({where: {id: id}});

    if(!getuser) {
        res.status(200).json({
            status : false,
            message: "No Data Found.",
            data: user,
        });
    }

    res.status(200).json({
        status : true,
        message: "User Updated Successfully",
        data: getuser,
    });
}

exports.deleteUser = async function (req, res) {
    const id = req.params.id;

    const user = await User.destroy({where: {id: id}});
    if (user) {
        
        res.status(200).json({
            status : true,
            message: "User Deleted Successfully.",
        });

    } else {
        res.status(200).json({
            status : false,
            message: "No Data Found.",
            data: user,
        });
    }
}


async function generate_password(password) {
    return md5.hex_md5(password);
}

async function compareIt(password) {
    const validPassword = await bcrypt.compare(password, hashedPassword);
}

//userslist  at admin side
exports.users_list = async function (req, res) {
    const user = await User.findAll();
    return res.send(user);
  }
  
exports.users_delete = async function (req,res) {
    const id = req.params.id;

    const user = await User.destroy({where: {id: id}});
    if (user) {
        return  res.status(200).json({
            success: true,
            message: "User Deleted Successfully.",
            data: user,
          });

    } else {
        return res.status(400).send("User Not Found.")
    }
};

exports.create = async function (req, res) {
    try{
        //console.log("req.file.filename", req.body.image.filename);
    let data = {
        first_name: req.body.first_name,
        last_name: req.body.last_name,
        contact_no: Number(req.body.contact_no),
        email: req.body.email,
        gender: req.body.gender,
        image: req.body.image,
        image_url: req.body.image_url,
        country_id:req.body.country_id,
        zipcode:req.body.zipcode,
       // image: req.body.image.filename,   //if image have to save with upload then you will get name of file here
        password: md5.hex_md5(req.body.password),
    }

    const user = await User.create(data);
    return  res.status(200).json({
        success: true,
        message: "User Created Successfully.",
        data: user,
      });

    }catch(error){
        res.status(500).json({
            success: false,
            message: error.message
        })
    }
};

//For saving image in folder 
exports.upload_file = async function (req,res)  {
    console.log("uploadfileurl", req.file);
    return  res.status(200).json({
        success: true,
        message: "image save in folder Successfully.",
        data: req.file,
      });
}