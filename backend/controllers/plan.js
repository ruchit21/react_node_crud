const db = require("../models");
const await = require("await");

const Plan = db.plan_model;

exports.getPlans = async function (req, res) {
    const plans = await Plan.findAll()

    if (!plans) {
        res.status(200).json({
            status: false,
            message: "No Data Found.",
            data: plans,
        });
    }

    res.status(200).json({
        status: true,
        message: "Plans get Successfully",
        data: plans,
    });
}

exports.getPlan = async function (req, res) {
    const id = req.params.id;

    const plan = await Plan.findOne({ where: { id: id } });
    if (plan) {
        res.json({ success: true, data: plan, message: "Plan data get successfully" })
    } else {
        res.json({ status: false, message: "Plan not found" })
    }
}

exports.addPlan = async function (req, res) {
    try {
        let data = {
            plan_type: req.body.plan_type,
            title: req.body.title,
            description: req.body.description,
            price: req.body.price,
            plan_duration: req.body.plan_duration,
            duration_type: req.body.duration_type,
            type: req.body.type,
            status: req.body.status
        }

        const plan = await Plan.create(data);

        res.status(200).json({
            success: true,
            message: "Plan Created Successfully",
            data: plan,
        });

    } catch (error) {
        res.status(500).json({
            message: error,
            success: false,
        });
    }
}

exports.updatePlan = async function (req, res) {
    try {
        
        const id = req.params.id;

        let data = {
            plan_type: req.body.plan_type,
            title: req.body.title,
            description: req.body.description,
            price: req.body.price,
            plan_duration: req.body.plan_duration,
            duration_type: req.body.duration_type,
            type: req.body.type,
            status: req.body.status,
        }
        const plan = await Plan.update(req.body, { where: { id: id } });
        return res.json({
            success: true,
            data: data,
            message: "Plan updated successfully"
        });

    } catch (error) {
        res.status(500).json({
            message: error,
            success: false,
        });
    }
}

exports.deletePlan = async function (req, res) {
    try {
        const id = req.params.id;

        const plan = await Plan.destroy({where: {id: id}});

        if (plan) {
            return res.json({ success: true, message: "Plan deleted successfully" });
        } else {
            return res.json({ success: false, message: "Plan not found." })
        }
    } catch (error) {
        res.status(500).json({
            message: error,
            success: false,
        });
    }
}

exports.changePlanStatus = async function (req, res) {
    try {
        const id = req.params.id;

        const plan = await Plan.findOne({ where: { id: id } });

        plan_status = plan.dataValues.status == 1 ? '0' : '1';
        const status_plan = await Plan.update({status: plan_status, updatedAt: new Date()}, { where: { id: id } });

        plan_data = plan;
        plan_data.status = plan_status;
        
        return res.json({
            success: true,
            data: plan_data,
            message: "Status updated successfully"
        });
    } catch (error) {
        res.status(500).json({
            message: error,
            success: false,
        });
    }
}