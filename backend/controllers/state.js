const db = require("../models");
const await = require("await");
const State = db.state_model;

exports.getStates = async function (req, res) {
    const id = req.params.id;
    const data = await State.findAll({where: {country_id: id}});

    if(!data) {
        res.json({
            status : false,
            message: "No states available.",
            data: data,
        });
    }
    res.json({
        status : true,
        message: "State data get successfully",
        data: data,
    });
}

exports.getState = async function (req, res) {
    try {
        
        const id = req.params.id;

        const state = await State.findOne({where: {id: id}});

        if (state) {
            res.json({
                status : true,
                message: "State data get successfully",
                data: state,
            });
        } else {
            res.json({
                status : false,
                message: "State not available",
            });
        }
    } catch (error) {
        res.status(500).json({
            message: error,
            success: false,
        });
    }
}

exports.updateState = async function (req, res) {
    try {
        const id = req.params.id;
        let data = {
            id: req.body.id,
            name: req.body.name,
            country_id: req.body.country_id,
            status: req.body.status
        }
        const state = await State.update(req.body, {where: {id: id}});

        if (!state) {
            res.json({
                success : false,
                message: "No states available.",
                data: data,
            });
        }
        res.json({
            success : true,
            message: "State updated successfully",
            data: data,
        });
    } catch (error) {
        res.status(500).json({
            message: error,
            success: false,
        });
    }
}

exports.createState = async function (req, res) {
    try {
        const data = {
            name: req.body.name,
            country_id: req.body.country_id,
            status: req.body.status
        }
        const state = await State.create(data);

        if (!state) {
            res.json({
                success : false,
                message: "No states available.",
                data: data,
            });
        }
        res.json({
            success : true,
            message: "State created successfully",
            data: data,
        });

    } catch (error) {
        res.status(500).json({
            message: error,
            success: false,
        });
    }
}

exports.deleteState = async function (req, res) {
    try {
        const id = req.params.id;

        const state = await State.destroy({where: {id: id}});
        console.log("state", state);

        if (!state) {
            res.json({
                success: false,
                message: "State not found."
            })
        }
        res.json({
            success: true,
            message: "State deleted successfully",
        });
    } catch (error) {
        res.status(500).json({
            message: error,
            success: false,
        });
    }
}

exports.getStateName = async function (req, res) {
    try {
        const id = req.params.id;

        const state = await State.findOne({where: {id: id}});

        if (!state) {
            res.json({
                success: false,
                message: "State not found."
            })
        }
        res.json({
            success: true,
            message: "State found successfully",
            data: state
        })
    } catch (error) {
        res.status(500).json({
            success: false,
            message: error.message
        })
    }
}

exports.changeStateStatus = async function (req, res) {
    try {
        const id = req.params.id;

        const state = await State.findOne({ where: { id: id } });

        state_status = state.dataValues.status == 1 ? '0' : '1';
        const state_plan = await State.update({status: state_status}, { where: { id: id } });

        state_data = state;
        state_data.status = state_status;
        
        return res.json({
            success: true,
            data: state_data,
            message: "Status updated successfully"
        });
    } catch (error) {
        res.status(500).json({
            message: error,
            success: false,
        });
    }
}