const db = require("../models");
const await = require("await");
const City = db.city_model;

exports.getCities = async function (req, res) {
    try {
        const id = req.params.id;

        const cities = await City.findAll({where: {state_id: id}});

        if (!cities) {
            res.send({
                success: false,
                message: "Cities not available"
            })
        }
        res.send({
            success: true,
            message: "Cities Found Successfully",
            data: cities
        })
    } catch (error) {
        res.status(500).json({
            success: false,
            message: error.message
        })
    }
}

exports.getCity = async function (req, res) {
    try {
        const id = req.params.id;

        const city = await City.findOne({where: {id: id}});

        if (!city) {
            res.json({
                success: false,
                message: "City not found",
            })
        }
        res.json({
            success: true,
            message: "City found successfully",
            data: city
        })
    } catch (error) {
        res.status(500).json({
            success: false,
            message: error.message
        })
    }
}

exports.createCity = async function (req, res) {
    try {
        const data = {
            name: req.body.name,
            state_id: req.body.state_id,
            status: req.body.status
        }

        const city = await City.create(data);

        if (!city) {
            res.send({
                success: false,
                message: "City not created something went wrong"
            })
        }
        res.json({
            success: true,
            message: "City created successfully",
            data: data
        })
    } catch (error) {
        res.status(500).json({
            success: false,
            message: error.message
        })
    }
}

exports.updateCity = async function (req, res) {
    try {
        const id = req.params.id;

        const data = {
            name: req.body.name,
            state_id: req.body.state_id,
            status: req.body.status
        }

        const city = await City.update(req.body, {where: {id: id}});

        if (!city) {
            res.json({
                success: false,
                message: "City not found"
            })
        }
        res.json({
            success: true,
            message: "City update successfully",
            data: data
        })
    } catch (error) {
        res.status.json({
            success: false,
            message: error.message
        })
    }
}

exports.deleteCity = async function (req, res) {
    try {
        const id = req.params.id;

        const city = await City.destroy({where: {id: id}});

        if (!city) {
            res.json({
                success: false,
                message: "City not found"
            })
        }
        res.json({
            success: true,
            message: "City deleted successfully"
        })
    } catch (error) {
        res.status(500).json({
            success: false,
            message: error.message
        })
    }
}

exports.changeCityStatus = async function (req, res) {
    try {
        const id = req.params.id;
        console.log("id", id);

        const city = await City.findOne({ where: { id: id } });

        city_status = city.dataValues.status == 1 ? '0' : '1';
        const city_plan = await City.update({status: city_status}, { where: { id: id } });

        city_data = city;
        city_data.status = city_status;

        return res.json({
            success: true,  
            data: city_data,
            message: "Status updated successfully"
        });
    } catch (error) {
        res.status(500).json({
            message: error,
            success: false,
        });
    }
}