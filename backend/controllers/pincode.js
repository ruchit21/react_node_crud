const db = require("../models");
const await = require("await");
const Pincode = db.pincode_model;
const City = db.city_model;


exports.getAllCityList = async (req, res) => {
    
    const data = await City.findAll();

    if(!data) {
        res.status(200).json({
            status : false,
            message: "No data fond.",
            data: data,
        });
    }
    res.status(200).json({
        status : true,
        message: "Data get successfully",
        data: data,
    });
};


exports.Create = async (req, res) => {
    try{
       
        const city_id = req.body.vals.city_id;
        let pincodes  = [];
        pincodes      = req.body.inputFields;
        let pin_length =  pincodes.length;

        for ( i = 0; i < pin_length; i++) {
            const data = await Pincode.create({
                pincode : pincodes[i].pincode,
                city_id : city_id,
            });
        }
          
        res.status(200).json({
            status : true,
            message: "Created Successfully",
            success: true,
        });

    }catch(error){
        res.status(500).json({
            success: false,
            message: error.message
        })
    }
};

exports.Update = async (req, res) => {
    try{
        const id = req.params.id;
        const data = await Pincode.update(req.body.vals, {where: {id: id}});

        res.status(200).json({
            status : true,
            success: true,
            message: "Update Successfully",
            data: data,
        });
    }catch(error){
        res.status(500).json({
            success: false,
            message: error.message
        })
    }
};

exports.List = async (req, res) => {
    
    const data = await Pincode.findAll();

    if(!data) {
        res.status(200).json({
            status : false,
            message: "No data fond.",
            data: data,
        });
    }
    res.status(200).json({
        status : true,
        message: "Data get successfully",
        data: data,
    });
};

exports.Delete = async (req, res) => {
    
    const id = req.params.id;

    const data = await Pincode.destroy({where: {id: id}});
    res.status(200).json({
        status : true,
        message: "Deleted Successfully.",
    });
};

exports.getpincode = async(req,res) =>{
    try{
        const id = req.params.id;
        console.log("id", id);

        const data = await Pincode.findOne({where :{city_id: id}});
        console.log("data pincode data", data);

        if(!data) {
        res.status(200).json({
            status : false,
            message: "No data fond.",
            data: data,
        });
        }else{
            res.status(200).json({
            status : true,
            message: "Data get successfully",
            data: data,
        });
            
        }

    }catch(error){
        res.status(500).json({
            success: false,
            message: error.message
        })
    }
}