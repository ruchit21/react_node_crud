const db = require("../models");
const await = require("await");
const Setting = db.setting_model;

exports.Create = async (req, res) => {
    const data = await Setting.create(req.body);
    res.status(200).json({
        status : true,
        message: "Created Successfully",
        data: data,
    });
};

exports.Update = async (req, res) => {
    const id = req.params.id;
    
    const data = await Setting.update(req.body, {where: {id: id}});

    res.status(200).json({
        status : true,
        message: "Update Successfully",
        data: data,
    });
};

exports.List = async (req, res) => {
    
    const data = await Setting.findAll();

    if(!data) {
        res.status(200).json({
            status : false,
            message: "No data fond.",
            data: data,
        });
    }
    res.status(200).json({
        status : true,
        message: "Data get successfully",
        data: data,
    });
};

exports.Delete = async (req, res) => {
    
    const id = req.params.id;

    const data = await Setting.destroy({where: {id: id}});
    res.status(200).json({
        status : true,
        message: "Deleted Successfully.",
    });
};