const db = require("../models");
const config = require("../config/admin-config");
// const commonService = require("../services/common.serivce");
const md5 = require("react-native-md5");
const User = db.user_model;
// const Role = db.role;
// const Admin = db.admin;
const sequelize = db.sequelize;
const _ = require("lodash");

const Op = db.Sequelize.Op;
// var mysql = require('mysql');
// const sql = require("mssql");
var jwt = require("jsonwebtoken");
var bcrypt = require("bcryptjs");

exports.login = async (req, res) => {

    User.findOne({
        where: {
          email: req.body.email
        }
      })
      .then(user => {
        if (!user) {
          return res.status(404).json({
            success: false,
            message: "User not found!"
          });
        }

        if (user.password != md5.hex_md5(req.body.password)) {
          return res.status(401).json({
            success: false,
            message: "Invalid Password!"
          });
        }
  
        var token = jwt.sign({
          id: user.id
        }, config.secret, {
          expiresIn: 86400 // 24 hours
        });
  
        res.status(200).json({
          success: true,
          data: {
            user: {
              name: user.first_name + " " + user.last_name,
              first_name : user.first_name,
              last_name : user.last_name,
              email : user.email,
              contact_no : user.contact_no,
              id: user.id
            },
            token: token
          },
        });
      })
  };

exports.update = async function (req, res) {
  const id = req.params.id;

  User.findOne({
    where: {
      id: id
    }
  })
  .then(user => {
    if (!user) {
      return res.status(404).json({
        success: false,
        message: "User not found!"
      });
    }

    data = {
      name: req.body.first_name + ' ' + req.body.last_name,
      first_name: req.body.first_name,
      last_name: req.body.last_name,
      contact_no: req.body.contact_no,
      email: req.body.email,
      id: req.body.id
    }
    const user_data = User.update(req.body, {where: {id: id}})

    res.status(200).json({
      success: true,
      message: "User Updated Successfully.",
      data: data,
    });
  })
};