const db = require("../models");
const await = require("await");
const Country = db.country_model;

exports.getCountries = async function (req, res) {
    const data = await Country.findAll({where: {status: 1}});

    if(!data) {
        res.status(200).json({
            status : false,
            message: "No countries available.",
            data: data,
        });
    }
    res.status(200).json({
        status : true,
        message: "Country data get successfully",
        data: data,
    });
}

exports.getAllCountries = async function (req, res) {
    const data = await Country.findAll();

    if(!data) {
        res.status(200).json({
            status : false,
            message: "No countries available.",
            data: data,
        });
    }
    res.status(200).json({
        status : true,
        message: "Country data get successfully",
        data: data,
    });
}

exports.getCountry = async function (req, res) {
    const id = req.params.id;
    const country = await Country.findOne({where: {id: id}})
    return res.send(country);
}

exports.updateCountry = async function (req, res) {
    const id = req.params.id;
    let data = {
        country_code: req.body.country_code,
        id: req.body.id,
        name: req.body.name,
        status: req.body.status,
        visible: true
    }
    const country = await Country.update(req.body, {where: {id: id}});
    return res.json({success: true, data: data, message: "Country updated successfully"});
}


exports.deleteCountry = async function (req, res) {
    const id = req.params.id;

    const response = await Country.destroy({where: {id: id}});
    if (response) {
        return res.json({ success: true, message: "Country deleted successfully" });
    } else {
        return res.json({ success: false, message: "Country not found." })
    }
}

exports.createCountry = async function (req, res) {
    try {
        let data = {
            country_code: req.body.country_code,
            name: req.body.name,
            status: req.body.status
        }

        const country = await Country.create(data);

        res.status(200).json({
            success: true,
            message: "Country Created Successfully",
            data: country,
        });

    } catch (error) {
        res.status(500).json({
            message: error,
            success: false,
        });
    }
}

exports.changeCountryStatus = async function (req, res) {
    try {
        const id = req.params.id;

        const country = await Country.findOne({ where: { id: id } });

        country_status = country.dataValues.status == 1 ? '0' : '1';
        const country_plan = await Country.update({status: country_status}, { where: { id: id } });

        country_data = country;
        country_data.status = country_status;
        
        return res.json({
            success: true,
            data: country_data,
            message: "Status updated successfully"
        });
    } catch (error) {
        res.status(500).json({
            message: error,
            success: false,
        });
    }
}

exports.getCountryName = async function (req, res) {
    try {
        const id = req.params.id;
        const country_name = await Country.findOne({where: {id: id}});
        if (country_name) {
            return res.json({
                success: true,
                data: country_name,
                message: "Country Find successfully"
            });
        } else {
            return res.json({
                success: false,
                message: "Country not available"
            });
        }
    } catch (error) {
        res.status(500).json({
            message: error,
            success: false,
        });
    }
}

