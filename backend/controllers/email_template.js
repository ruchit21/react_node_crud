const db = require("../models");
const await = require("await");
const EmailTemplate = db.email_template_model;

exports.CreateEmailTemplate = async (req, res) => {
    // let data = {
    //     email_template_name:     req.body.email_template_name,
    //     email_status:           req.body.email_status,
    //     email_slug:             req.body.email_slug,
    //     email_content:          req.body.email_content,
    //     email_subject:          req.body.email_subject,
    //     email_available_tags:   req.body.email_available_tags,
    // }
    // console.log("data", data);
    
    const emailTemplate = await EmailTemplate.create(req.body);

    res.status(200).json({
        status : true,
        message: "Email Template Created Successfully",
        data: emailTemplate,
    });
};

exports.UpdateEmailTemplate = async (req, res) => {
    const id = req.params.id;
    
    const emailTemplate = await EmailTemplate.update(req.body, {where: {id: id}});

    res.status(200).json({
        status : true,
        message: "Email Template Update Successfully",
        data: emailTemplate,
    });
};

exports.ListEmailTemplate = async (req, res) => {
    
    const data = await EmailTemplate.findAll();

    if(!data) {
        res.status(200).json({
            status : false,
            message: "No email template available.",
            data: data,
        });
    }
    res.status(200).json({
        status : true,
        message: "email template data get successfully",
        data: data,
    });
};

exports.DeleteEmailTemplate = async (req, res) => {
    
    const id = req.params.id;

    const data = await EmailTemplate.destroy({where: {id: id}});
    res.status(200).json({
        status : true,
        message: "Email Template Deleted Successfully.",
    });
};

exports.ChangeStatus = async (req, res) => {
    const id = req.params.id;
    let status_change = (req.body.email_status == "active") ? "inactive" : "active"; 
    let status  = {
        email_status: status_change,
    }
    const data = await EmailTemplate.update(status, {where: {id: id}});
    const getdata = await EmailTemplate.findOne({where: {id: id}});
    res.status(200).json({
        status : true,
        data: getdata,
        message: "Email Template " + status_change + " Successfully.",
    });
};