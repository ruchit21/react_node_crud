const router = require("express").Router();
const controller = require("../controllers/email_template");

router.post("/create", controller.CreateEmailTemplate);
router.post("/changeStatus/:id", controller.ChangeStatus);
router.get("/list", controller.ListEmailTemplate);
router.delete("/delete/:id", controller.DeleteEmailTemplate);
router.put("/update/:id", controller.UpdateEmailTemplate);

module.exports = router
