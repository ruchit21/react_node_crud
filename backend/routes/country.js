const country_controller = require("../controllers/country");
const router = require("express").Router();

router.get('/list', country_controller.getCountries);
router.get('/allcountrieslist', country_controller.getAllCountries);
router.get('/getcountry/:id', country_controller.getCountry);
router.put('/update/:id', country_controller.updateCountry);
router.put('/delete/:id', country_controller.deleteCountry);
router.post('/create', country_controller.createCountry);
router.post('/changestatus/:id', country_controller.changeCountryStatus);
router.get('/getcountryname/:id', country_controller.getCountryName);

module.exports = router
