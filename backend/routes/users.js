const user_controller = require("../controllers/users");
const multer = require("multer");
const path = require("path");
const router = require('express').Router();

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, path.join(__dirname,'../images'))
    },
    filename: function (req, file, cb) {
        const uniqueSuffix = Date.now() + path.extname(file.originalname);
        cb(null, file.fieldname + '-' + uniqueSuffix);
    }
})

const upload = multer({
    storage: storage,
    limits: {fileSize: '1000000'},
    fileFilter: (req, file, cb) => {
        const fileTypes = /jpeg|jpg|png|gif/
        const mimeType = fileTypes.test(file.mimetype)
        const extname = fileTypes.test(path.extname(file.originalname))

        if (mimeType && extname) {
            return cb(null, true)
        }
        cb('Give Proper File Formate To Upload')
    }
}).single("image")

router.post('/insert', upload, user_controller.createUser);
router.get('/get', user_controller.getUsers);
router.get('/getuser/:id', user_controller.getUser);
router.put('/update/:id', user_controller.updateUser);
router.put('/delete/:id', user_controller.deleteUser);

//new api 
router.get("/manage-user",user_controller.users_list)
router.delete("/delete/:id",user_controller.users_delete)
router.post('/create', user_controller.create);
router.get('/alldata/:id', user_controller.getUser);
router.put('/update/:id', user_controller.updateUser);

router.post('/upload-file',upload,user_controller.upload_file);         //For uploading image in folder
module.exports = router