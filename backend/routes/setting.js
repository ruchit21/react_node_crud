const router = require("express").Router();
const controller = require("../controllers/setting");

router.post("/create", controller.Create);
router.get("/list", controller.List);
router.delete("/delete/:id", controller.Delete);
router.put("/update/:id", controller.Update);

module.exports = router
