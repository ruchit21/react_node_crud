const city_controller = require("../controllers/City");
const router = require("express").Router();

router.get('/getcities/:id', city_controller.getCities);
router.get('/getcity/:id', city_controller.getCity);
router.put('/update/:id', city_controller.updateCity);
router.post('/create', city_controller.createCity);
router.put('/delete/:id', city_controller.deleteCity);
router.post('/changestatus/:id', city_controller.changeCityStatus);

module.exports = router
