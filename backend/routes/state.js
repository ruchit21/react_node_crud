const state_controller = require("../controllers/state");
const router = require("express").Router();

router.get('/getstates/:id', state_controller.getStates);
router.get('/getstate/:id', state_controller.getState);
router.put('/update/:id', state_controller.updateState);
router.post('/create', state_controller.createState);
router.put('/delete/:id', state_controller.deleteState);
router.get('/getstatename/:id', state_controller.getStateName);
router.post('/changestatus/:id', state_controller.changeStateStatus);

module.exports = router
