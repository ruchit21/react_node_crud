const plan_controller = require("../controllers/plan");
const router = require("express").Router();

router.get('/getallplans', plan_controller.getPlans);
router.get('/getplan/:id', plan_controller.getPlan);
router.post('/changestatus/:id', plan_controller.changePlanStatus);
router.put('/update/:id', plan_controller.updatePlan);
router.post('/create', plan_controller.addPlan);
router.put('/delete/:id', plan_controller.deletePlan);

module.exports = router
