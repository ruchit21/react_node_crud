const router = require("express").Router();
const controller = require("../controllers/pincode");

router.post("/create", controller.Create);
router.get("/list", controller.List);
router.delete("/delete/:id", controller.Delete);
router.put("/update/:id", controller.Update);
router.get("/city-list",controller.getAllCityList);
router.get("/get-pincode/:id",controller.getpincode);

module.exports = router
