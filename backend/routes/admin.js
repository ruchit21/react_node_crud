const router = require("express").Router();
const controller = require("../controllers/admin");

router.post("/login", controller.login);

router.put("/update/:id", controller.update);

module.exports = router
