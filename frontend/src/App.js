import {BrowserRouter, Routes, Route} from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import './App.css';
import Home from './pages/Home';
import AddEdit from './pages/AddEdit';
import View from './pages/View';
import Login from "./pages/Login";
import Signup from "./pages/Signup";

function getToken() {
  const tokenString = sessionStorage.getItem('token');
  return tokenString
}

function App() {
  const token = getToken();

  if (!token) {
    return (
      <BrowserRouter>
        <div className='App'>
          <ToastContainer position='top-center'/>
          <Routes>
            <Route exact path="/" element={<Login/>}/>
            <Route exact path="/login" element={<Login/>}/>
            <Route path="/signup" element={<Signup/>}/>
            {/* <Login setToken={setToken} /> */}
          </Routes>
        </div>
      </BrowserRouter>)
  }

  return(
    <BrowserRouter>
      <div className='App'>
        <ToastContainer position='top-center'/>
        {/* <Home /> */}
        <Routes>
          <Route exact path="/" element={<Home/>}/>
          <Route path="/login" element={<Login/>}/>
          <Route path="/addUser" element={<AddEdit/>}/>
          <Route path="/update/:id" element={<AddEdit/>}/>
          <Route path="/view/:id" element={<View/>}/>
        </Routes>
      </div>
      </BrowserRouter>
  )
}

export default App