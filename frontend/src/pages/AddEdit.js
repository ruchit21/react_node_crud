import React, {useState, useEffect} from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import './../App.css';
import axios from 'axios';
import { toast } from "react-toastify";
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormControl from '@mui/material/FormControl';
import FormLabel from '@mui/material/FormLabel';
import Stack from '@mui/material/Stack';
import { styled } from '@mui/material/styles';
import GLOBAL from '../Globals';

const Input = styled('input')({
  display: 'none',
});

const initialiState = {
  first_name: "",
  last_name: "",
  contact_no: "",
  email: "",
  gender: "",
}

const AddEdit = () => {
  const [state, setState] = useState(initialiState);
  const [image, handleImageUpload] = useState('');

  const {first_name, last_name, contact_no, email, gender, password} = state;

  const navigate = useNavigate();
  
  const {id} = useParams();
  
  useEffect(() => {
    if (id) {
      getSingleUser(id);
    }
  }, [id]);

  const getSingleUser = async (id) => {
    const response = await axios.get(GLOBAL.BACKEND_URL + `/user/getuser/${id}`);
    if (response.status === 200) {
      setState({...response.data});
    }
  }

  const addUser = async (data) => {
    const response = await axios.post(GLOBAL.BACKEND_URL + `/user/insert`, data);
    if (response.status === 200) {
      toast.success(response.data);
    }
  }

  const updateUser = async (data, id) => {
    const response = await axios.put(GLOBAL.BACKEND_URL + `/user/update/${id}`, data);
    if (response.status === 200) {
      toast.success(response.data);
    }
  }

  const handleSubmit = (e) => {
    e.preventDefault();
    if (!first_name || !last_name || !contact_no || !email || !gender) {
      toast.error("Please Provide All Required Fields.");
    } else {
      if (!id) {
        const formData = new FormData()

        formData.append('first_name', state.first_name);
        formData.append('last_name', state.last_name)
        formData.append('contact_no', state.contact_no)
        formData.append('email', state.email)
        formData.append('gender', state.gender)
        formData.append('password', state.password)
        formData.append('image', image)

        addUser(formData);
      } else {
        updateUser(state, id);
      }
      setTimeout(() => navigate("/"), 500);
    }
  }

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setState({...state, [name]: value});
  }

  return (
    <div style={{padding: "10px"}}>
        <form onSubmit={handleSubmit} method="POST" encType='multipart/form-data'>
          <div className='user-form'>
            <h2 className='text-center m-0 border mb-5'> {id ? "Update" : "Add"} User </h2>

            <TextField id="first_name" label="First Name" name='first_name' placeholder='Enter First name' value={first_name} onChange={handleInputChange}/>

            <TextField id="last_name" label="Last Name" name='last_name' placeholder='Enter Last name' value={last_name} onChange={handleInputChange}/>

            <TextField id="contact_no" label="Contact No" name='contact_no' placeholder='Enter Contact no' value={contact_no} onChange={handleInputChange}/>

            <TextField id="email" label="Email" name='email' placeholder='Enter email ' value={email} onChange={handleInputChange}/> 

            <TextField id="password" label="Password" name='password' placeholder='Enter Password ' value={password} onChange={handleInputChange}/> 

            <Stack className='mb-5' direction="row" alignItems="center" spacing={2}>
              <label htmlFor="contained-button-file">
                <Input accept="image/*" name='image' onChange={(e) => handleImageUpload(e.target.files[0])} id="contained-button-file"  type="file" />
                <Button variant="contained" component="span">
                  Upload Image
                </Button>
              </label>
            </Stack>

            <FormControl>
              <FormLabel id="demo-row-radio-buttons-group-label">Gender</FormLabel>
                <RadioGroup
                  row
                  aria-labelledby="demo-row-radio-buttons-group-label"
                  value={gender}
                  name="row-radio-buttons-group"
                >
                <FormControlLabel name='gender' value="male" control={<Radio />} onChange={handleInputChange} label="Male" />
                <FormControlLabel name='gender' value="female" control={<Radio />} onChange={handleInputChange} label="Female" />
                <FormControlLabel name='gender' value="other" control={<Radio />} onChange={handleInputChange} label="Other" />
              </RadioGroup>
            </FormControl>

            <Button type='submit' className='mb-5' variant="contained" color='success'>{id ? "Update" : "Save"}</Button>
            <Button variant="contained" href="/" color='primary'>Go Back</Button>

          </div>
        </form>   
    </div>
  )
} 

export default AddEdit