import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import axios from 'axios';
import '../App.css';
import Button from '@mui/material/Button';
import GLOBAL from '../Globals';

const View = () => {
    const [user, setUser] = useState(null);

    const { id } = useParams();

    useEffect(() => {
        if (id) {
            getSingleUser(id);
        }
    }, [id]);

    const getSingleUser = async (id) => {
        const response = await axios.get(GLOBAL.BACKEND_URL + `/user/getuser/${id}`);
        if (response.status === 200) {
            setUser({ ...response.data });
        }
    }
    return (
        <div style={{ padding: "10px" }}>
            <div className='user-form'>
                <h2 className='text-center m-0 border mb-5'> {user && user.first_name + ' ' + user.last_name} Details </h2>
                <div className='d-flex align-items-center'>
                    <div className='title_div'>
                        <h5><b> Image : </b></h5>
                    </div>
                    <div className='value_div'>
                        <h5> {user && <img src={require(`../../src/images/${user.image}`)} height="70px" width="70px" alt='user image'/>} </h5>
                    </div>
                </div>
                <div className='user_details d-flex'>
                    <div className='title_div'>
                        <h5><b> Id : </b></h5>
                        <h5><b> First name : </b></h5>
                        <h5><b> Last name : </b></h5>
                        <h5><b> Contact No : </b></h5>
                        <h5><b> Email : </b></h5>
                        <h5><b> Gender : </b></h5>
                    </div>
                    <div className='value_div'>
                        <h5> {id} </h5>
                        <h5> {user && user.first_name} </h5>
                        <h5> {user && user.last_name} </h5>
                        <h5> {user && user.contact_no} </h5>
                        <h5> {user && user.email} </h5>
                        <h5> {user && user.gender} </h5>
                    </div>
                </div>
                <Button variant="contained" href="/" color='primary'>Go Back</Button>
            </div>
        </div>
    )
}

export default View