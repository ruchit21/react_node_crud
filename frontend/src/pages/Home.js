import { useState, useEffect } from 'react';
import './../App.css';
import { toast } from 'react-toastify';
import axios from "axios";
import Button from '@mui/material/Button';
import IconButton from '@mui/material/Button';

import { styled } from '@mui/material/styles';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import Tooltip from '@mui/material/Tooltip';
import GLOBAL from '../Globals';

//Icons
import PersonAddAltIcon from '@mui/icons-material/PersonAddAlt';
import EditIcon from '@mui/icons-material/Edit';
import LogoutIcon from '@mui/icons-material/Logout';
import DeleteIcon from '@mui/icons-material/Delete';
import VisibilityIcon from '@mui/icons-material/Visibility';

const Home = () => {
    const [data, setData] = useState([]);

    const loadData = async () => {
        const response = await axios.get(GLOBAL.BACKEND_URL + "/user/get");
        setData(response.data);  
    };
    useEffect(() => {
        loadData();
    }, []);

    const deleteLink = (id) => {
        if (window.confirm("Are you sure you want to delete that user...?")) {
            axios.put(GLOBAL.BACKEND_URL + `/user/delete/${id}`);
            toast.success("User Deleted Successfully");
            setTimeout(() => window.location.reload(false), 2000);
        }
    }

    const updateLink = (id) => {
        if (id) {
            window.location.href = GLOBAL.FRONTEND_URL + '/update/' + id;
        }
    }

    const viewLink = (id) => {
        if (id) {
            window.location.href = GLOBAL.FRONTEND_URL + '/view/' + id;
        }
    }

    const logoutLink = (id) => {
        if (window.confirm("Are you sure you want to logout ?")) {
            const tokenString = sessionStorage.removeItem('token');
            toast.success("User logout Successfully");
            setTimeout(() => window.location.reload(false), 2000);
        }
    }

    const StyledTableCell = styled(TableCell)(({ theme }) => ({
        [`&.${tableCellClasses.head}`]: {
          backgroundColor: theme.palette.common.black,
          color: theme.palette.common.white,
        },
        [`&.${tableCellClasses.body}`]: {
          fontSize: 14,
        },
      }));
      
      const StyledTableRow = styled(TableRow)(({ theme }) => ({
        '&:nth-of-type(odd)': {
          backgroundColor: theme.palette.action.hover,
        },
        // hide last border
        '&:last-child td, &:last-child th': {
          border: 0,
        },
      }));

    return (
        <div style={{padding: "10px"}}>
            <div className='mb-5' style={{textAlign: "right"}}>
                <Button variant="contained" href='addUser' color='primary' className='m-5' startIcon={<PersonAddAltIcon />}> Add User </Button>
                <Button variant="contained" color='secondary' 
                    className='m-5' startIcon={<LogoutIcon/>}
                    onClick={() => logoutLink()} > LogOut </Button>
            </div>
            <TableContainer component={Paper}>
            <Table sx={{ minWidth: 700 }} aria-label="customized table">
                <TableHead>
                <TableRow>
                    <StyledTableCell align="center">Id</StyledTableCell>
                    <StyledTableCell align="center">Image</StyledTableCell>
                    <StyledTableCell align="center">Firstname</StyledTableCell>
                    <StyledTableCell align="center">Lastname</StyledTableCell>
                    <StyledTableCell align="center">Contact No</StyledTableCell>
                    <StyledTableCell align="center">Email</StyledTableCell>
                    <StyledTableCell align="center">Gender</StyledTableCell>
                    <StyledTableCell align="center">Action</StyledTableCell>
                </TableRow>
                </TableHead>
                <TableBody>
                {data.map((row) => (
                    <StyledTableRow key={row.id}>
                        <StyledTableCell component="th" scope="row" align="center">
                            {row.id}
                        </StyledTableCell>
                        <StyledTableCell align="center"><img src={require(`../../src/images/${row.image}`)} alt="user image" height="100px" width="100px"/></StyledTableCell>
                        <StyledTableCell align="center">{row.first_name}</StyledTableCell>
                        <StyledTableCell align="center">{row.last_name}</StyledTableCell>
                        <StyledTableCell align="center">{row.contact_no}</StyledTableCell>
                        <StyledTableCell align="center">{row.email}</StyledTableCell>
                        <StyledTableCell align="center">{row.gender}</StyledTableCell>
                        <StyledTableCell align="center">
                            <Tooltip title="Edit">
                                <IconButton onClick={() => updateLink(row.id)} color='primary'>
                                    <EditIcon />
                                </IconButton>
                            </Tooltip>
                            <Tooltip title="Delete">
                                <IconButton onClick={() => deleteLink(row.id)} color='error'>
                                    <DeleteIcon />
                                </IconButton>
                            </Tooltip>
                            <Tooltip title="View">
                                <IconButton onClick={() => viewLink(row.id)} color='success'>
                                    <VisibilityIcon />
                                </IconButton>
                            </Tooltip>
                        </StyledTableCell>
                    </StyledTableRow>
                ))}
                </TableBody>
            </Table>    
            </TableContainer>
        </div>
    )
}

export default Home