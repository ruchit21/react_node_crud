import React, { useState } from "react";
import { Navigate } from "react-router-dom";
import axios from 'axios';
import { toast } from "react-toastify";
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import GLOBAL from '../Globals';
import "../App.css";

export default function Login() {

  const [email, setEmail, redirect] = useState("");
  const [password, setPassword] = useState("");


  function validateForm() {
    return email.length > 0 && password.length > 0;
  }
  
  function getToken() {
    const tokenString = sessionStorage.getItem('token');
    return tokenString
  }

  const send_req = async (data) => {
    const response = await axios.post(GLOBAL.BACKEND_URL + `/user/login`, data);
    if (response.status === 200) {
      if (response.data.success) {
        const tokenString = sessionStorage.setItem('token', response.data.data.password);
        toast.success(response.data.message);
        setTimeout(() => window.location.reload(false), 2000);
      } else {
        toast.error(response.data.message);
      }

    } else {
      toast.error(response.data);
    }
  }

  const handleSubmit = (e) => {
    e.preventDefault();
    var data = {
      email: email,
      password: password,
    }
    send_req(data);
  }

  const token = getToken();
  if (token) {
    return <Navigate to="/"/>
  }

  return (
    
    <div className="Login">
      <form onSubmit={handleSubmit}>
        <div className='user-form'>
          <h2 className='text-center m-0 border mb-5'> Login </h2>
          <TextField id="email" label="Email" name='email' placeholder='Enter First name' value={email} onChange={(e) => setEmail(e.target.value)}/>
          
          <TextField id="password" label="Password" name='password' placeholder='Enter First name' value={password} onChange={(e) => setPassword(e.target.value)}/>
          
          <Button type='submit' className='mb-5' variant="contained" color='success' disabled={!validateForm()}>Login</Button>
          <Button variant="contained" href='signup' color='secondary' className='m-5'> Signup </Button>
        </div>
      </form>
    </div>
  );
}